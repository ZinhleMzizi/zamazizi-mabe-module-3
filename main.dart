 import 'package:flutter/material.dart';
import 'package:flutter_application_1/forgot_password.dart';
import 'package:flutter_application_1/home_screen.dart';
import 'package:flutter_application_1/login_screen.dart';
import 'package:flutter_application_1/signup_screen.dart';
import 'package:flutter_application_1/routes.dart';

void main() => runApp(const AuthApp());

class AuthApp extends StatelessWidget {
  const AuthApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        MyRoutes.homeScreen: (context) => const HomeScreen(),
        MyRoutes.loginScreen: (context) => const Login_screen(),
        MyRoutes.signUp: (context) => const SignUp(),
        MyRoutes.forgotPassword: (context) => const ForgotPassword(),
      },
    );
  }
}